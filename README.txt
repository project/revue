CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides integration with Revue (https://www.getrevue.co), an
editorial newsletter tool for writers and publishers.

The module enables site builders to place a subscription form block where
visitors can subscribe to their Revue newsletter.

 * For a full description of the module visit:
   https://www.drupal.org/project/revue

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/revue


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Revue module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.

CONFIGURATION
-------------

Navigate to Administration > Structure > Block layout and select "Place block"
in the region of your choice. In the "Place block" modal find the block named
"Revue Subscription block", click "Place block" and configure the block.

You need an API key from Revue to get a working subscription form. You can find
your Revue API at https://www.getrevue.co/app/integrations at the bottom of the
page if you are logged in to your Revue account.

MAINTAINERS
-----------

 * Martijn Vermeulen (Marty2081) - https://www.drupal.org/u/marty2081

Supporting organizations:

 * LimoenGroen - https://www.drupal.org/limoengroen
 * KRO-NCRV - https://www.drupal.org/kro-ncrv
