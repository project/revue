<?php

namespace Drupal\revue;

/**
 * Class RevueApiException.
 *
 * @package Drupal\revue
 */
class RevueApiException extends \Exception {}
